#Old Designs

![Heco Inmobiliaria](https://bytebucket.org/eloypineda/code-examples/raw/a38491a5a4204419d45897014a1de82de42ef91e/old-designs/hecoinmobiliaria.png)

![MyMagnetism](https://bytebucket.org/eloypineda/code-examples/raw/a38491a5a4204419d45897014a1de82de42ef91e/old-designs/mymagnetism.png)

![PCGO](https://bytebucket.org/eloypineda/code-examples/raw/a38491a5a4204419d45897014a1de82de42ef91e/old-designs/franquicias_pcgo.png)

![Sevilla en Red](https://bytebucket.org/eloypineda/code-examples/raw/a38491a5a4204419d45897014a1de82de42ef91e/old-designs/portal_web_sevillaenred.png)

![Feria y Fiestas de Consolación](https://bytebucket.org/eloypineda/code-examples/raw/a38491a5a4204419d45897014a1de82de42ef91e/old-designs/FeriaUtrera2003_JPEG.jpg)

![Utrera Joven](https://bytebucket.org/eloypineda/code-examples/raw/a38491a5a4204419d45897014a1de82de42ef91e/old-designs/utrerajoven_boceto_02.jpg)

![Cartel oncierto The Vagabonds](https://bytebucket.org/eloypineda/code-examples/raw/a38491a5a4204419d45897014a1de82de42ef91e/old-designs/banner_platanosonico_20040514_TheVagabonds_BusStop_72pix.jpg)

![Cartel concierto Funkestein](https://bytebucket.org/eloypineda/code-examples/raw/a38491a5a4204419d45897014a1de82de42ef91e/old-designs/banner_platanosonico_20040506_Funkestein_a_NoLyrics.jpg)

![Cartel concierto Maggot Brain](https://bytebucket.org/eloypineda/code-examples/raw/a38491a5a4204419d45897014a1de82de42ef91e/old-designs/banner_platanosonico_20040416_MaggotBrain_BusStop.jpg)

![Cartel concierto Smoking Bird](https://bytebucket.org/eloypineda/code-examples/raw/a38491a5a4204419d45897014a1de82de42ef91e/old-designs/banner_platanosonico_20031223_SmokingBird_BusStop.jpg)

![Cartel CineClub Plátano Sónico](https://bytebucket.org/eloypineda/code-examples/raw/a38491a5a4204419d45897014a1de82de42ef91e/old-designs/banner_platanosonico_20030123_cineclubPS_JPEG.jpg)

![Cartel concierto Utopia Factory](https://bytebucket.org/eloypineda/code-examples/raw/a38491a5a4204419d45897014a1de82de42ef91e/old-designs/banner_cafebarcentral_20060501_utopiafactory.jpg)
