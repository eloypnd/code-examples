### Code examples
---

In this repository you can find three folders with three different small projects where you can check some of my coding:

* **TV Guide for Tunedin** in [tunnedin](https://bitbucket.org/eloypineda/code-examples/src/e3422dad5cdbab51c96dfd09341c9dbf58acc91a/tunnedin) folder, a small TV guide aplication using meteor framework.
* **Timeline for Simplesurance** in [simplesurance](https://bitbucket.org/eloypineda/code-examples/src/e3422dad5cdbab51c96dfd09341c9dbf58acc91a/simplesurance) folder, playing a bit with Zend Framework and Bootstrap.
* And a simple example of **plain HTML/CSS from PSD** in [XHTMLized](https://bitbucket.org/eloypineda/code-examples/src/e3422dad5cdbab51c96dfd09341c9dbf58acc91a/XHTMLized) folder, coded for XHTMLized.
* Some [old designs](https://bitbucket.org/eloypineda/code-examples/src/5ee1adf39327b0c49c5deb5d2d6f09909bc7ada4/old-designs/)
