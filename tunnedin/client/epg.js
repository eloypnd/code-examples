var requestingBroadcasts = false;
var broadcast_width = 812;

Channels = new Meteor.Collection('channels');
Broadcasts = new Meteor.Collection('broadcasts');

Meteor.subscribe('channels');
Meteor.subscribe('broadcasts');

Template.channels.channels = function () {
  return Channels.find({});
}

Template.broadcasts.channels = function () {
  return Channels.find({});
}
Template.broadcasts.broadcasts = function (channel_id) {
  return Broadcasts.find({channel_id: channel_id});
}
Template.broadcasts.events({
  'scroll .channelbc': function (element) {
    var containerWidth = element.currentTarget.offsetWidth;
    var contentWidth = element.currentTarget.scrollWidth;
    var scrollPosition = element.currentTarget.scrollLeft;
    var maxScroll = contentWidth - 800;
    if (containerWidth + scrollPosition > maxScroll && !requestingBroadcasts) {
      requestingBroadcasts = true;
      Meteor.call('getNextBroadcasts', function (err, result) {
        requestingBroadcasts = false;
      })
    }
  }
});

Template.broadcast.rendered = function (what) {
  var n_childs = this.firstNode.parentElement.childElementCount;
  this.firstNode.parentElement.style.width = n_childs * broadcast_width + 'px';

  console.log('Template rendered');
}
Template.broadcast.timeFormat = function (datetime) {
  dateToFormat = new Date(datetime * 1000);
  return dateToFormat.getHours() + ':' + ('0' + dateToFormat.getMinutes()).slice(-2);
}
