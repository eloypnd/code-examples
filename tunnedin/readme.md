##TV Guide

for tunedin

---

Create an EPG (Electronic Programming Guide) using HTML, CSS3 and Javascript.

The EPG should fetch all channels and display their broadcasts. When the user scrolls to the end of a channel’s broadcasts it should fetch more.

**Used for this project**:

* [Meteor](http://www.meteor.com/) is an open-source platform base on JavaScript and nodejs.
* HTML5/CSS3
* [less](http://lesscss.org/) is a CSS3 pre-processor.
