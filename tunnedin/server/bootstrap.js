Meteor.startup(function () {
  Channels.remove({});
  Broadcasts.remove({});

  api_response = TunnedinAPI.fetch({'limit': 5});

  _.each(api_response.data.data, function (channel) {
    var channel_id = Channels.insert(channel.channel);
    _.each(channel.broadcasts, function (broadcast) {
      broadcast.channel_id = channel_id;
      Broadcasts.insert(broadcast);
    });
  });

});