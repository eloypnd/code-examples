Channels = new Meteor.Collection('channels');
Broadcasts = new Meteor.Collection('broadcasts');

Meteor.methods({
  getNextBroadcasts: function (options) {
    // TODO: check and validate params
    var start_time = 0;
    // First we find out from wich time we have to fetch the broadcast from the API
    // We will use the minimun end time form all channels last broadcast
    Channels.find({}).forEach(function (channel) {
      broadcast = Broadcasts.findOne({channel_id: channel._id}, {sort: {'time.end': -1}, fields: {'time.end': 1}});
      if (broadcast.time.end < start_time || start_time === 0) {
        start_time = broadcast.time.end;
      }
    });
    // Fetch next 5 broadcasts from this time
    api_response = TunnedinAPI.fetch({'limit': 5, 'start': start_time});

    // Add the broadcasts to the collection
    _.each(api_response.data.data, function (channel) {
      var channel_id = Channels.findOne({id: channel.channel.id}, {fields: {_id: 1}});
      _.each(channel.broadcasts, function (broadcast) {
        // need to check that the program doesn't exists already
        if (_.isEmpty(Broadcasts.findOne({'broadcast.id': broadcast.broadcast.id}))) {
          broadcast.channel_id = channel_id._id;
          Broadcasts.insert(broadcast);
        }
      });
    });
  }
});

// Publish complete set of channels to all clients.
Meteor.publish('channels', function () {
  return Channels.find();
});

Meteor.publish('broadcasts', function () {
  return Broadcasts.find();
});
